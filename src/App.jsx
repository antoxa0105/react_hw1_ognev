import React from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import modalWindowDeclarations from './components/modalConfig';
import './App.css'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      modalState: {
        modalId: 0,
        modalHeader: 'title',
        modalText: 'text',
        backgroundColor: '',
        closeBtn: false
    }      
     }
  }

  handleModal = (e) => {
    const modalId = e.target.dataset.id;
    const dataForModal = modalWindowDeclarations.find(item => item.id === +modalId);
    this.setState({
      showModal: true,
      modalState: {
        modalId: dataForModal.id,
        header: dataForModal.header,
        text: dataForModal.text,
        backGroundColor: dataForModal.backGroundColor,
        closeBtn: dataForModal.closeBtn,
        action1:dataForModal.action1,
        action2:dataForModal.action2,
        classNameBtn1: dataForModal.classNameBtn1,
        classNameBtn2: dataForModal.classNameBtn2
    }
    })
  }

  closeModal = () => {
    this.setState({showModal: false});
  }

  render() { return (
    <div className="App">
      <main className="main">
      <Button
       modalId={1}
       btnClassName={"button"}
       onClick={this.handleModal}
       text={'Open first modal'}
       backgroundColor={'rgba(255, 0, 0, 0.8)'}
       />

      <Button
       modalId={2}
       btnClassName={"button"}
       onClick={this.handleModal}
       text={'Open Second modal'}
       backgroundColor={'rgba(0, 255, 0, 0.8)'}
       />
      {
        this.state.showModal && <Modal 
        onClick={this.closeModal}
        closeBtn={this.state.modalState.closeBtn} 
        header={this.state.modalState.header}
        text={this.state.modalState.text}
        backgroundColor={this.state.modalState.backGroundColor}
        actions={
          <>
            <Button
              btnClassName={this.state.modalState.classNameBtn1}
              text={this.state.modalState.action1}
            />
            <Button
               btnClassName={this.state.modalState.classNameBtn2}
               text={this.state.modalState.action2}
               onClick={this.closeModal}
            />
          </>
        }      
        />
      }
      
      </main>
      </div>
  );
  }
}
