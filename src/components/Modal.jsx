import React from "react";
import './Modal.scss'
 

export default class Modal extends React.Component {
      render() {
        return (
          <>
             <div className="modal" onClick={this.props.onClick}>
             <div className="modal-content" style={{backgroundColor: this.props.backgroundColor}} onClick={ e => e.stopPropagation()}>
                 <div className="modal-content__header">{this.props.header}
                 {this.props.closeBtn  && <span className="modal-content__close-btn-cross" onClick={this.props.onClick}></span>}
                 </div>                 
                 <p className="modal-content__text">{this.props.text}</p>
                 <div className="btn-container">
                 {this.props.actions}
                 </div>
             </div>
           </div>
          </>
        )
      }

}
