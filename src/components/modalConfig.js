
const modalWindowDeclarations = [
    {
      id: 1,
      header: 'Do you want to delete this file?',
      text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cum est eum incidunt voluptate. Error dolorum adipisci velit eveniet assumenda. Sequi.',
      backGroundColor: 'rgba(255, 0, 0, 0.8)',
      closeBtn: true,
      action1:"Apply",
      action2:"Dismiss",
      classNameBtn1: 'button apply',
      classNameBtn2: 'button dismiss',

    },
    {
      id: 2,
      header: "You can save this file",
      text: 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Но парадигматическая взобравшись пунктуация, речью переулка ему использовало города злых.',
      backGroundColor: 'rgba(0, 255, 0, 0.8)',
      closeBtn: false,
      action1:"Confirm",
      action2:"Discard",
      classNameBtn1: 'button confirm',
      classNameBtn2: 'button discard',
    }
  ]

export default modalWindowDeclarations;
