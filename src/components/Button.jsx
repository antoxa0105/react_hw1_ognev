import React from "react";
import "./Button.scss"


export default class Button extends React.Component {  
 render() {
   return (
    <>
    <button
     className={this.props.btnClassName}
     data-id={this.props.modalId}
     onClick={this.props.onClick}
     style={{backgroundColor: this.props.backgroundColor}}>
     {this.props.text}
    </button>
    </>
    )
  }
}


